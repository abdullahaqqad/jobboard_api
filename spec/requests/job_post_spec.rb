require 'rails_helper'
require 'cancan/matchers'

RSpec.describe 'JobBoard API', type: :request do
  # initialize test data
  let(:user) { create(:user, admin: true) }
  let(:normal_user) { create(:user, admin: false) }
  let(:header) { { 'Authorization' => token_generator(user.id) } }
  let(:header_normal_user) { { 'Authorization' => token_generator(normal_user.id) } }

  let!(:job_posts) { create_list(:job_post, 10, user_id: user.id) }
  let(:job_post_id) { job_posts.first.id }

  # Test suite for GET /todos
  describe 'GET /job_posts' do
    # make HTTP get request before each example

    # GET Index
    context 'when the request with authentication header' do
      before { get '/job_posts', headers: header }
      it 'returns job_posts' do
        expect(json).not_to be_empty
        expect(json.size).to eq(10)
      end
      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end
    end
    context 'when the request with NO authentication header' do
      before { get '/job_posts' }
      it 'returns status code 401' do
        expect(response).to have_http_status(401)
      end
    end

    # GET show
    context 'when the request to show admin job posts' do
      before { get '/job_posts/1', headers: header }
      it 'returns status code OK 200' do
        expect(response).to have_http_status(200)
      end
    end
  end
  # POST /job_posts
  describe 'POST /job_posts' do
    context 'when the request with admin' do
      before { post '/job_posts', params: { title: 'test', description: 'test' }, headers: header }
      it 'creates job_posts' do
        expect(json).not_to be_empty
        expect(response).to have_http_status(201)
      end
    end
    context 'when the request with normal user' do
      it 'creates job_posts' do
        expect { post '/job_posts', params: { title: 'test', description: 'test' }, headers: header_normal_user }.to raise_error(CanCan::AccessDenied)
      end
    end
  end

end

