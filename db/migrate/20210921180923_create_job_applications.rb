class CreateJobApplications < ActiveRecord::Migration[6.1]
  def change
    create_table :job_applications do |t|
      t.string :created_by
      t.string :status, default: 'not_seen'
      t.references :job_post, null: false, foreign_key: true

      t.timestamps
    end
  end
end
