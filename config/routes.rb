Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  post 'signin', to: 'authentication#authenticate'
  post 'signup', to: 'user#create'

  Rails.application.routes.draw do
    resources :job_posts do
      resources :job_applications
    end
  end
end
