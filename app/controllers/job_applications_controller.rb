class JobApplicationsController < ApplicationController
  include Response
  include ExceptionHandler
  load_and_authorize_resource
  before_action :set_job_post
  before_action :set_job_application, only: %i[show update destroy]

  # GET /job_posts/:job_post_id/job_applications
  def index
    @job_application = JobApplication.all
    json_response(@job_application)
  end

  # POST /job_posts/:job_post_id/job_applications
  def create
    @job_application = current_user.job_applications.create!(job_application_params)
    json_response(:created)
  end

  # GET /job_posts/:job_post_id/job_applications/:id
  def show
    if current_user.admin? && @job_post.user_id == current_user.id
      @job_application.status = 'seen'
      @job_application.save
    end
    json_response(@job_application)
  end

  def update
    if @job_application.created_by == current_user.id
      @job_application.update(job_application_params)
      json_response(@job_application, :update)
      head :no_content
    else
      json_response('you dont have access to the job application', :unauthorized)
    end
  end

  def destroy
    if @job_application.created_by == current_user.id
      @job_application.destroy
      json_response(:destroyed)
      head :no_content
    else
      json_response('you dont have access to the job application', :unauthorized)
    end
  end

  private

  def job_application_params
    # whitelist params
    params.permit(:status, :job_post_id, :created_by)
  end

  def set_job_post
    @job_post = JobPost.find(params[:job_post_id])
  end

  def set_job_application
    @job_application = @job_post.job_applications.find(params[:id]) if @job_post
  end

end
