class UserController < ApplicationController
  load_and_authorize_resource
  skip_before_action :authenticate_request, only: [:create]

  def create
    if !User.find_by_email(user_params[:email])
      @user = User.create!(user_params)
      json_response(:created)
    else
      json_response(:already_exists)
    end
  end

  private

  def user_params
    # whitelist params
    params.permit(:email, :password, :admin)
  end

end
