class User < ApplicationRecord
  has_secure_password
  has_many :job_applications, foreign_key: :created_by
  has_many :job_posts, foreign_key: :user_id
  validates_presence_of :email, :password_digest
  validates :admin, acceptance: { accept: [true, false] }
end
